const init = require("../init");
const postgresConnector = require("../../src/dataAccess/postgresConnector");

let agent;
let users;

const TAG_ID = "00000000-5de1-308c-d13e-6410b804ac89";

const ROUTE = "/tag";

describe("Tag", function () {
    this.timeout(100000);

    before((done) => {
        init()
            .then((object) => {
                agent = object.agent;
                users = object.users;
            })
            .then(() => done())
            .catch((err) => done(err));
    });

    beforeEach(async () => {
        await postgresConnector.getModel("metadata").truncate({ cascade: true });
        await postgresConnector.getModel("tag").truncate({ cascade: true });
        await postgresConnector.getModel("tag").create({
            id: TAG_ID,
            title: "Test",
        });
        await postgresConnector.getModel("metadata").create(
            {
                type: "map",
                component: "mapComponent",
                //route from dummy strategy
                route: "/parkings",
                client_route: "/parkings",
                title: "Nadpis 1",
                description: "Popis 1",
                metadataTags: [{ tag_id: TAG_ID }],
            },
            { include: "metadataTags" }
        );
    });

    it("GET /user/1/tag should return 400 no access token present", function (done) {
        agent.get("/user/1/tag").expect(400, function (err, res) {
            done(err);
        });
    });

    it("GET /user/1/tag should return 401 unauthorized", function (done) {
        agent
            .get("/user/1/tag")
            .set("x-access-token", "tttttt")
            .expect(401, function (err, res) {
                done(err);
            });
    });

    it("GET /user/2/tag should return 403 no access to foreign tag", function (done) {
        agent
            .get("/user/2/tag")
            .set("x-access-token", users.user1.token)
            .expect(403, function (err, res) {
                done(err);
            });
    });

    it("GET /user/1/tag should return 200 with empty array", function (done) {
        agent
            .get("/user/1/tag")
            .set("x-access-token", users.user1.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(0);
                done(err);
            });
    });

    it("GET /user/2/tag should return 200", function (done) {
        agent
            .get("/user/2/tag")
            .set("x-access-token", users.user2.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(1);
                res.body[0].should.have.property("_id", TAG_ID);
                res.body[0].should.have.property("title", "Test");
                done(err);
            });
    });

    it("GET /tag should return 200 nothing stored", (done) => {
        postgresConnector
            .getModel("tag")
            .truncate({ cascade: true })
            .then(() => {
                agent
                    .get(ROUTE)
                    .set("x-access-token", users.user2.token)
                    .expect(200, (err, res) => {
                        res.body.should.be.instanceof(Array).and.have.lengthOf(0);
                        res.body.should.be.empty();
                        done(err);
                    });
            });
    });

    it("POST /tag should return 403 forbidden for non admin", (done) => {
        const res = agent
            .post(ROUTE)
            .set("content-type", "application/json")
            .set("x-access-token", users.user1.token)
            .send(
                JSON.stringify({
                    title: "Test 2",
                })
            )
            .expect(403, (err, res) => {
                res.body.should.have.property("error");
                done(err);
            });
    });

    it("POST /tag should return 201 and increment order of new tag", (done) => {
        agent
            .post(ROUTE)
            .set("content-type", "application/json")
            .set("x-access-token", users.user2.token)
            .send(
                JSON.stringify({
                    title: "Test 2",
                })
            )
            .expect(201, (err, res) => {
                res.body.should.have.property("title", "Test 2");
                done(err);
            });
    });

    it("GET /tag should return 200", (done) => {
        agent
            .get(ROUTE)
            .set("x-access-token", users.user2.token)
            .expect(200, (err, res) => {
                res.body.should.be.instanceof(Array).and.have.lengthOf(1);
                res.body[0].should.have.property("title", "Test");
                done(err);
            });
    });

    it("GET /tag/:id should return 200", (done) => {
        agent
            .get(`${ROUTE}/${TAG_ID}`)
            .set("x-access-token", users.user2.token)
            .expect(200, (err, res) => {
                res.body.should.have.property("title", "Test");
                done(err);
            });
    });

    it("PUT /tag/id should return 200", async () => {
        const putRes = await agent
            .put(`${ROUTE}/${TAG_ID}`)
            .set("content-type", "application/json")
            .set("x-access-token", users.user2.token)
            .send(
                JSON.stringify({
                    title: "Upravený test",
                })
            );

        putRes.should.have.property("statusCode", 204);

        const getRes = await agent
            .get(`${ROUTE}/${TAG_ID}`)
            .set("content-type", "application/json")
            .set("x-access-token", users.user2.token)
            .send();

        getRes.should.have.property("statusCode", 200);
        getRes.body.should.have.property("title", "Upravený test");
    });

    it("DELETE /tag/{tagId} should return 204 should delete tag", function (done) {
        agent
            .delete(`${ROUTE}/${TAG_ID}`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .send()
            .expect(204, function (err, res) {
                postgresConnector
                    .getModel("tag")
                    .findAll({ where: { id: TAG_ID } })
                    .then((tag) => {
                        tag.should.be.instanceOf(Array).and.lengthOf(0);
                        done(err);
                    })
                    .catch(done);
            });
    });

    it("GET /tag/nonsense should return 400", (done) => {
        agent
            .get(`${ROUTE}/nonsense`)
            .set("x-access-token", users.user2.token)
            .expect(400, (err, res) => {
                done(err);
            });
    });

    it("GET /tag/:wrongId should return 404", (done) => {
        const wrongId = [6, ...TAG_ID.substring(1)].join("");
        agent
            .get(`${ROUTE}/${wrongId}`)
            .set("x-access-token", users.user2.token)
            .expect(404, (err, res) => {
                done(err);
            });
    });

    it("DELETE /tag/:wrongId should return 404", (done) => {
        const wrongId = [6, ...TAG_ID.substring(1)].join("");
        agent
            .delete(`${ROUTE}/${wrongId}`)
            .set("x-access-token", users.user2.token)
            .expect(404, (err, res) => {
                done(err);
            });
    });
});
