const request = require("supertest");
const should = require("should");
const init = require("../init");

let agent;
let testObj = {
    name: "John",
    surname: "Doe",
    email: "john@doe.com",
    organization: "Operator ICT",
};

describe("AccessRequest", function () {
    this.timeout(100000);

    before(function (done) {
        init().then((object) => {
            agent = object.agent;
            done();
        });
    });

    it("Responds with 222 - Error while validating user data", function (done) {
        testObj.email = "";
        agent
            .post("/accessrequest")
            .send(testObj)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(422)
            .end(function (err, res) {
                res.body.should.have.property("isOk", false);
                res.body.errors.should.have.property("email");
                if (err) return done(err);
                return done();
            });
    });

    it("Responds with 200 - Successful sending of the letter", function (done) {
        agent
            .post("/accessrequest")
            .send({
                name: "John",
                surname: "Doe",
                email: "john@doe.com",
                organization: "Operator ICT",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                return done();
            });
    });
});
