```mermaid
sequenceDiagram
    participant FE as Frontend CP
    participant ACL as Permission proxy
    participant BE as Backend CP
    participant PBI as Power BI
    participant OA as Output api

    FE->>ACL: Login
    Note left of FE: Zobrazení seznamu <br>možností a metdat
    ACL->>FE:JWT obsahující routelist
    FE->>BE: metadata
    BE->ACL: pouze ověření JWT
    BE->>FE: metadata pro uživatele

    Note left of FE: Kliknutí na dashboard
    FE->>ACL: click dashboard/456
    ACL->>PBI: stahni Power BI
    PBI->>ACL: 
    ACL->>FE: vizualizace na proxy url

    Note left of FE: Kliknutí na mapu
    FE->>ACL: click lampskarlin/list
    ACL->>OA: lampy karlin + limitation: praha X
    OA->>ACL: API response 
    ACL->>FE: data
```