# Client Panel BE

> :warning: This service has been archived and is **no longer maintained**.

Refer to the API documentation at:

-   [Main (production) Golemio BI API Documentation](https://clientpanelbackend.docs.apiary.io/#)
-   [Test (development) Golemio BI API Documentation](https://clientpanelbackenddev.docs.apiary.io/#)

> :warning: The Apiary documentation is deprecated and **no longer maintained**. OpenAPI specification will be provided soon, see [issue #42](https://gitlab.com/operator-ict/golemio/code/golemio-bi/backend/-/issues/42).

## Functions BE

Solution serves as an API to save in a structured format dashboard related information:

-   dashboard metadata
-   files attachment/thumbnails are saved in GeeToo S3
-   tags to filter dashboards according categories
-   favourite dashboards for users
-   additional configuration data for dashboards

## Example of dashboard metadata structure

```json
{
    "_id": "00000000-6011-39b2-a382-a678ba6c85f3",
    "title": "Mapa stanovišť na tříděný odpad",
    "description": "Obsahuje všechny stanoviště v Praze, lze filtrovat pouze nádoby osazené senzorem pro měření zaplněnosti.",
    "route": "/v2/sortedwastestations",
    "client_route": "/sortedwastestations",
    "type": "application",
    "component": "sortedWasteMap",
    "data": { "districtsRoute": "/v2/citydistricts", "measurementsRoute": "/v2/sortedwastestations/measurements" },
    "thumbnail": { "content_type": "image/png", "url": "/metadata/00000000-6011-39b2-a382-a678ba6c85f3/thumbnail" },
    "tags": [{ "_id": "00000000-6011-c128-c2af-490013b81e7b", "title": "Odpady" }]
}
```

## Testing

Backend has tests covering most of the API endpoints. Mocked database is used during testing, there is no need to change config.

-   Install dependencies `yarn install`
-   Run tests `yarn coverage`

Coverage report can be opened with `coverage/lcov-report/index.html`.

#### Env variable notation:

```
PORT - port on which the application is listening for incoming requests.
LOG_LEVEL - logger settings, which levels should be logged (error, warn, info, verbose, debug, silly)

AUTH_STRATEGY - strategy used for authentication [golemio, dummy] (dummy - user information from [strategy/dummy](https://gitlab.com/operator-ict/golemio/code/client-panel-backend/blob/development/src/modules/authentication/strategy/dummy.js), golemio - auth server)
AUTH_URL - url to auth server (permission proxy)

POSTGRES_CONN - connection string for postgres database
POSTGRES_POOL_MAX_CONNECTIONS - how many concurent connections can be opened

POWER_BI_CLIENT_ID -
POWER_BI_USERNAME -
POWER_BI_PASSWORD -

MAILER_ENABLED - request emails for account creation enabled
MAILER_HOST - smtp host for sending emails
MAILER_PORT - port of smtp
MAILER_USERNAME - login to smtp
MAILER_PASSWORD - password to smtp
MAILER_FROM - what reciever see in sender field
MAILER_TO - responsible endpoint for handling account creation requests



S3_ENDPOINT -
S3_REGION - (internal note: use 'prague' for geetoo s3)
S3_ACCESS_KEY_ID -
S3_SECRET_ACCESS_KEY -
S3_BUCKET_NAME -
S3_UPLOAD_PART_SIZE - The size in bytes for each individual part to be uploaded. Adjust the part size to ensure the number of parts does not exceed maxTotalParts. See 5mb is the minimum allowed part size.
S3_UPLOAD_QUEUE_SIZE - The size of the concurrent queue manager to upload parts in parallel. Set to 1 for synchronous uploading of parts. Note that the uploader will buffer at most queueSize * partSize bytes into memory at any given time.
S3_ATTACHMENTS_PATH - destination where to save attachments in bucket
S3_THUMBNAILS_PATH - destination where to save thumbnails in bucket
```

## API specification

OpenAPI specification will be provided soon.

## Logging

Backend is using [winston](https://www.npmjs.com/package/winston) library for logging which is highly customizable and there are also a lot of extensions. Currently the API is sending all logs on the stdout.

## Migrations

Solution contains sql scripts for creating/removing related database tables. For that purpose an internal tool [@golemio/cli](https://www.npmjs.com/package/@golemio/cli) is used. It is part of developer dependencies and can be used after instalation of neccesarry libraries (see chapter Testing). Also it is required to define variable POSTGRES_CONN in a .env file.

Command for creating neccesary database structure:

```bash
yarn golemio migrate-db up --postgres
```

Command for removing related db structure:

```bash
yarn golemio migrate-db down --postgres
```
