ARG BASE_IMAGE=node:18.17.0-alpine

FROM $BASE_IMAGE
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install && \
    yarn build-apidocs
COPY . .

# Remove busybox links
RUN busybox --list-full | \
    grep -E "bin/ifconfig$|bin/ip$|bin/netstat$|bin/nc$|bin/poweroff$|bin/reboot$" | \
    sed 's/^/\//' | xargs rm -f

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

# Disable persistent history
RUN touch /app/.ash_history && \
    chmod a=-rwx /app/.ash_history && \
    chown root:root /app/.ash_history

USER nonroot

EXPOSE 3000
CMD ["yarn", "start"]
