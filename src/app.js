require("bluebird-global");
const http = require("http");
const express = require("express");
const cookieParser = require("cookie-parser");
const path = require("path");
const cors = require("cors");
const expressValidator = require("express-validator");
const resources = require("./resources");
const policy = require("./modules/policy");
const APIError = require("./modules/error");
const Authentication = require("./modules/authentication");
const { validationOptions } = require("./modules/validation");
const getLogger = require("./modules/logger").getLogger;
const config = require("dotenv");
const PostgresConnector = require("./dataAccess/postgresConnector");

class App {
    running = false;

    constructor() {
        //
    }

    start = async () => {
        if (this.running) {
            return;
        }

        config.load({
            path: path.join(__dirname, "..", ".env"),
        });

        this.express = express();
        const tokenHeader = process.env.TOKEN_NAME || "x-access-token";
        this.express.set(
            "app_authentication",
            new Authentication(
                {
                    strategy: (process.env.NODE_ENV !== "test" && process.env.AUTH_STRATEGY) || "dummy",
                    authUrl: process.env.AUTH_URL,
                },
                getLogger("authentication")
            )
        );

        this.express.use(
            cors({
                credentials: true,
                exposedHeaders: ["X-Count", "X-Items-Per-Page", "X-Page"],
                allowedHeaders: [
                    "Origin",
                    "X-Requested-With",
                    "Content-Type",
                    "Accept",
                    "Access-Token",
                    "pragma",
                    "cache-control",
                    tokenHeader,
                ],
            })
        );

        this.express.use(expressValidator(validationOptions));
        this.express.use(express.urlencoded({ extended: true }));
        this.express.use(express.json());

        this.express.use(cookieParser());

        if (process.env.NODE_ENV === "test") {
            /*const mongoServer = new MongoMemoryServer();
            const mongoUri = await mongoServer.getUri();
            this.initMongo(mongoUri);*/

            if (!process.env.POSTGRES_CONN) {
                console.error(`Env variable POSTGRES_CONN is required`);
                process.exit(1);
            }
            await this.initPostgres(process.env.POSTGRES_CONN);
        } else {
            if (!process.env.POSTGRES_CONN) {
                console.error(`Env variable POSTGRES_CONN is required`);
                process.exit(1);
            }
            await this.initPostgres(process.env.POSTGRES_CONN);
        }

        policy.init({ token_name: tokenHeader }, this.express.get("app_authentication"), getLogger("policy"), APIError);

        /**
         * Create HTTP server.
         */
        this.server = http.createServer(this.express);
        resources(this.express);

        const port = this.normalizePort(process.env.PORT || "3000");
        this.express.set("port", port);

        /**
         * Listen on provided port, on all network interfaces.
         */
        this.server.listen(port);
        this.server.on("error", this.onError);
        this.server.on("listening", this.onListening);
        this.running = true;
    };

    /**
     * Event listener for HTTP server "error" event.
     */
    onError = (error) => {
        if (error.syscall !== "listen") {
            throw error;
        }

        const port = this.normalizePort(process.env.PORT || "3000");
        const bind = typeof port === "string" ? `Pipe ${port}` : `Port ${port}`;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case "EACCES":
                console.error(`${bind} requires elevated privileges`);
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(`${bind} is already in use`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    };

    /**
     * Event listener for HTTP server "listening" event.
     */
    onListening = () => {
        const addr = this.server.address();
        const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
        getLogger("app").info("listening", { bind });
    };

    /**
     * Normalize a port into a number, string, or false.
     */
    normalizePort = (val) => {
        const port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    };

    initPostgres = async (connString) => {
        return await PostgresConnector.connect(connString);
    };

    getExpress = () => {
        return this.express;
    };
}

module.exports = new App();
