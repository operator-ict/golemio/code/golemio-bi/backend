const { DataTypes } = require("sequelize");

const MetadataTagAttributeModel = {
    metadata_id: { type: DataTypes.UUID },
    tag_id: { type: DataTypes.UUID },
};

module.exports = MetadataTagAttributeModel;
