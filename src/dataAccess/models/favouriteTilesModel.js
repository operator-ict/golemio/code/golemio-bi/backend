const { DataTypes } = require("sequelize");

const FavouriteTilesAttributeModel =  {
    user_id: { type: DataTypes.UUID },
    metadata_id: { type: DataTypes.UUID },
};

module.exports = FavouriteTilesAttributeModel;
