const { DataTypes } = require("sequelize");

const UserAttributeModel = {
    id: { type: DataTypes.UUID, autoIncrement: true, primaryKey: true },
    user_id: { type: DataTypes.NUMBER },
};

module.exports = UserAttributeModel;
