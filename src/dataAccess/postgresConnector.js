const sequelize = require("sequelize");
const getLogger = require("../modules/logger").getLogger;
const initiateModels = require("../dataAccess/models/modelManager");

class MySequelize {
    connect = async (connectionString = process.env.POSTGRES_CONN) => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!connectionString) {
                getLogger("database").error("The ENV variable POSTGRES_CONN cannot be undefined.", err);
            }

            this.connection = new sequelize.Sequelize(connectionString, {
                define: {
                    freezeTableName: true,
                    timestamps: true, // adds createdAt and updatedAt
                    createdAt: "created_at",
                    updatedAt: "updated_at",
                    underscored: true, // automatically set field option for all attributes to snake cased name
                },
                logging: (msg) => getLogger("database").silly(msg), // logging by Logger::
                pool: {
                    acquire: 60000,
                    idle: 10000,
                    max: Number(process.env.POSTGRES_POOL_MAX_CONNECTIONS) || 10,
                    min: 0,
                },
                retry: {
                    match: [
                        /SequelizeConnectionError/,
                        /SequelizeConnectionRefusedError/,
                        /SequelizeHostNotFoundError/,
                        /SequelizeHostNotReachableError/,
                        /SequelizeInvalidConnectionError/,
                        /SequelizeConnectionTimedOutError/,
                        /TimeoutError/,
                    ],
                    max: 8,
                },
                omitNull: true,
            });

            if (!this.connection) {
                getLogger("database").error("Connection is undefined.", err);
                throw err;
            }

            await this.connection.authenticate();
            getLogger("database").info("Connected to PostgreSQL!");
            initiateModels(this.connection);
            return this.connection;
        } catch (err) {
            getLogger("database").error("Error while connecting to PostgreSQL.", err);
            throw err;
        }
    };

    getConnection = () => {
        if (!this.connection) {
            getLogger("database").error("Sequelize connection does not exist. First call connect() method.", err);
            throw err;
        }

        return this.connection;
    };

    isConnected = async () => {
        try {
            await this.connection.authenticate();
            return true;
        } catch (err) {
            return false;
        }
    };

    disconnect = async () => {
        getLogger("database").info("PostgreSQL disconnect called");
        if (this.connection) {
            await this.connection.close();
        }
    };

    getModel(name) {
        try {
            if (this.connection.models[name]) return this.connection.models[name];
            else throw new Error("Invalid model name");
        } catch (err) {
            getLogger("database").error(`Error while loading model: ${name}.`, err);
            throw err;
        }
    }
}

module.exports = new MySequelize();
