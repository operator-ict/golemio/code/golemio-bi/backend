const path = require("path");
const StorageProvider = require("./storageProvider");
const { configLoader } = require("../resources/accessrequest/configLoader");
const getLogger = require("../modules/logger").getLogger;

class StorageService {
    constructor() {
        this.storageClient = StorageProvider.getInstance(configLoader.storage, getLogger("storage"));
        this.attachmentsPath = configLoader.storage.attachmentsPath;
        this.thumbnailsPath = configLoader.storage.thumbnailsPath;
    }

    uploadAttachment = async (data, attachmentId, suffix) => {
        try {
            await this.storageClient.uploadFile(data, this.attachmentsPath, "", attachmentId + suffix);
            return path.join(this.attachmentsPath, attachmentId + suffix);
        } catch (err) {
            getLogger("storage").error(`Attachment upload for metadata: '${attachmentId}' was unsuccessful`, err);
            throw err;
        }
    };

    uploadThumbnail = async (data, metadataId, suffix) => {
        try {
            await this.storageClient.uploadFile(data, this.thumbnailsPath, "", metadataId + suffix);
            return path.join(this.thumbnailsPath, metadataId + suffix);
        } catch (err) {
            getLogger("storage").error(`Thumbnail upload for metadata: '${metadataId}' was unsuccessful`, err);
            throw err;
        }
    };

    getObjectAsStream = async (key) => {
        return this.storageClient.getFileStream(key);
    };

    deleteObjectByKey = async (key) => {
        return this.storageClient.deleteFile(key);
    };
}

module.exports = new StorageService();
