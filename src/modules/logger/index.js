const winston = require('winston');
const expressWinston = require('express-winston');

const myFormat = winston.format.printf(info => {
    const stringifiedRest = JSON.stringify(Object.assign({}, info, {
        level: undefined,
        message: undefined,
        splat: undefined,
        label: undefined,
        timestamp: undefined
    }));

    if (stringifiedRest !== '{}') {
        info = `${info.timestamp} [${info.level}] - (${info.label}) ${info.message} ${stringifiedRest}`;
    } else {
        info = `${info.timestamp} [${info.level}] - (${info.label}) ${info.message}`;
    }

    return info;
});

const createConsoleTransport = (program = 'default') =>
    new winston.transports.Console({
        colorize: true,
        prettyPrint: true,
        meta: true,
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.label({label: program}),
            winston.format.timestamp(),
            myFormat
        )
    });

expressWinston.requestWhitelist.push('session');
expressWinston.requestWhitelist.push('body');
expressWinston.bodyBlacklist.push('password');

const getLogger = (name) =>
    winston.createLogger({
        level: process.env.LOG_LEVEL || 'info',
        transports: [
            createConsoleTransport(name),
        ]
    });

module.exports.getLogger = getLogger;

module.exports.getRequestLogger = () =>
    expressWinston.logger({
        winstonInstance: getLogger('request')
    });

