const NodeCache = require("node-cache");

const getCacheStore = (stdTTL = 60) => {
    const myCache = new NodeCache({ stdTTL });
    const store = {
        get: (key) => {
            return myCache.get(key);
        },
        set: (key, value, ttl) => {
            myCache.set(key, value, ttl);
        },
        remove: (key) => {
            myCache.del(key);
        },
        has: (key) => {
            return myCache.has(key);
        }
    };

    return store;
}

module.exports = {
    getCacheStore,
};
