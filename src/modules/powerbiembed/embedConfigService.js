// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

const auth = require("./authentication");
const utils = require("./utils");
const PowerBiReportDetails = require("./models/embedReportConfig");
const EmbedConfig = require("./models/embedConfig");
const fetch = require('node-fetch');
const getLogger = require('../logger').getLogger;
const log = getLogger('powerbiembed:embedConfigService');
const getCacheStore = require('../cache').getCacheStore;
const cache = getCacheStore();

/**
 * Generate embed token and embed urls for reports
 * @return Details like Embed URL, Access token and Expiry
 */
async function getEmbedInfo(workspaceId, reportId) {

    // Get the Report Embed details
    try {
        // Get report details and embed token
        const embedParams = await getEmbedParamsForSingleReport(workspaceId, reportId);
        return {
            'accessToken': embedParams.embedToken.token,
            'embedUrl': embedParams.reportsDetail,
            'expiry': embedParams.embedToken.expiration,
            'status': 200
        };
    } catch (err) {
        log.error(err)
        return {
            'status': 500,
            'error': err.toString(),
        }
    }
}

/**
 * Get embed params for a single report for a single workspace
 * @param {string} workspaceId
 * @param {string} reportId
 * @param {string} additionalDatasetId - Optional Parameter
 * @return EmbedConfig object
 */
async function getEmbedParamsForSingleReport(workspaceId, reportId, additionalDatasetId) {
    const reportInGroupApi = `https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportId}`;
    const headers = await getRequestHeader();

    // Get report info by calling the PowerBI REST API
    const result = await fetch(reportInGroupApi, {
        method: 'GET',
        headers: headers,
    });

    if (!result.ok) {
        throw result;
    }

    // Convert result in json to retrieve values
    const resultJson = await result.json();

    // Add report data for embedding
    const reportDetails = new PowerBiReportDetails(resultJson.id, resultJson.name, resultJson.embedUrl);
    const reportEmbedConfig = new EmbedConfig();

    // Create mapping for report and Embed URL
    reportEmbedConfig.reportsDetail = [reportDetails];

    // Create list of datasets
    let datasetIds = [resultJson.datasetId];

    // Append additional dataset to the list to achieve dynamic binding later
    if (additionalDatasetId) {
        datasetIds.push(additionalDatasetId);
    }

    // Get Embed token multiple resources
    reportEmbedConfig.embedToken = await getEmbedTokenForSingleReportSingleWorkspace(headers, reportId, datasetIds, workspaceId);
    return reportEmbedConfig;
}

/**
 * Get Embed token for single report, multiple datasets, and an optional target workspace
 * @param {string} reportId
 * @param {Array<string>} datasetIds
 * @param {string} targetWorkspaceId - Optional Parameter
 * @return EmbedToken
 */
async function getEmbedTokenForSingleReportSingleWorkspace(headers, reportId, datasetIds, targetWorkspaceId) {

    // Add report id in the request
    let formData = {
        'reports': [{
            'id': reportId
        }]
    };

    // Add dataset ids in the request
    formData['datasets'] = [];
    for (const datasetId of datasetIds) {
        formData['datasets'].push({
            'id': datasetId
        })
    }

    // Add targetWorkspace id in the request
    if (targetWorkspaceId) {
        formData['targetWorkspaces'] = [];
        formData['targetWorkspaces'].push({
            'id': targetWorkspaceId
        })
    }

    const embedTokenApi = "https://api.powerbi.com/v1.0/myorg/GenerateToken";
    const start = performance.now();

    // Generate Embed token for single report, workspace, and multiple datasets. Refer https://aka.ms/MultiResourceEmbedToken
    const result = await fetch(embedTokenApi, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(formData)
    });
    const end = performance.now();
    log.debug(`Time taken to get embed token: ${end - start}ms`);

    if (!result.ok)
        throw result;
    return result.json();
}

/**
 * Get Request header
 * @return Request header with Bearer token
 */
async function getRequestHeader() {
    // Get the response from the authentication request
    try {
        let accessToken;
        const start = performance.now();
        if (cache.has("embedAccessToken")) {
            log.debug("Cache hit for access token");
            accessToken = cache.get("embedAccessToken");
        } else {
            log.debug("Cache miss for access token");
            const authResult = await auth.getAccessToken();
            accessToken = authResult.accessToken;
            // Store the token in cache with expiration time minus 60 seconds (1 minute)
            cache.set("embedAccessToken", accessToken, Math.round((authResult.expiresOn.valueOf() - (new Date()).valueOf()) / 1000) - 60);
        }
        const end = performance.now();
        log.debug(`Time taken to get access token: ${end - start}ms`);

        return {
            'Content-Type': "application/json",
            'Authorization': utils.getAuthHeader(accessToken)
        };
    } catch (err) {
        throw err;
    }
}

module.exports = {
    getEmbedInfo: getEmbedInfo
}
