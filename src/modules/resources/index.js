const fs = require('fs');
const path = require('path');

module.exports = (app, logger) => {
    const resourcePath = path.join(__dirname, '..', '..', 'resources');
    logger.info('Resource file load', {path: resourcePath, type: 'base_directory'});
    const folders = fs.readdirSync(resourcePath);
    let stat;
    folders.forEach((f) => {
        stat = fs.statSync(path.join(resourcePath, f));
        if (stat.isDirectory()) {
            let p = path.join(resourcePath, f, 'route.js');
            if (fs.existsSync(p)) {
                logger.debug('Resource file load', {resource: f, type: 'route', path: p});
                app.use(require(p));
            }
        }
    });
};
