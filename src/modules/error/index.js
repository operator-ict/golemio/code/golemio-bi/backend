const messages = require('./messages');

class APIError extends Error {
    constructor(message, status) {
        super(message);
        this.name = 'APIError';
        this.status = status || 500;
    }
}

module.exports = APIError;
module.exports.messages = messages;