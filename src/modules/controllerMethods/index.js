module.exports.decorateResponse = (res, data, options) => {
    if (options.limit && options.page) {
        res.set({
            'x-count': data.count,
            'x-items-per-page': options.limit,
            'x-page': options.page
        });
    }
    return res;
};

module.exports.decorateResponseAndSend = (res, options) => (result) => {
    res = this.decorateResponse(res, result, options);
    res.json(result.rows);
};