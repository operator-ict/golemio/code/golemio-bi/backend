function toDto(tagDao) {
    return {
        title: tagDao.title,
        _id: tagDao.id,
    };
}
function toDao(tagDto) {
    return {
        id: tagDto.id,
        title: tagDto.title,
    };
}
module.exports = { toDto, toDao };
