const APIError = require('../modules/error');
const {getLogger, getRequestLogger} = require('../modules/logger');
const { MulterError } = require("multer");

module.exports = (app) => {
    const log = getLogger('app:resources');

    app.use(getRequestLogger());

    const resources = require('../modules/resources');
    resources(app, log);

    app.use('*', (req, res) => {
        log.warn(`Non existing route: ( ${req.method} ) ${req.originalUrl}`);
        res.status(404).json({message: 'Route not found.'});
    });

    app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
        if (typeof err === 'number') {
            return res.sendStatus(err);
        }
        if (err instanceof APIError) {
            if (err.status < 400 || err.status > 499) {
                log.error('Error', err, err.stack);
            }
            return res.status(err.status).json({error: err.message});
        }
        if (err && err.name === 'SequelizeUniqueConstraintError') {
            return res.status(409).json({error: 'already_exist'});
        }

        if (err instanceof MulterError) {
            if (err.code === "LIMIT_FILE_SIZE") {
                return res.status(413).json({error: err.message});
            }
            return res.status(500).json({error: err.message});
        }

        log.error(err.message, {stack: err.stack});
        return res.status(500).json({error: 'server_error'});
    });
};
