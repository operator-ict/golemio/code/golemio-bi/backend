const {Router} = require('express');

// const forms = require('./forms');
const controller = require('./controller');
const policy = require('../../modules/policy');
// const auth = require('../../modules/authentication/constants');
// const validation = require('../../modules/validation');
// const checkErrors = require('../../modules/validation').checkErrors;

const router = new Router();
const baseUrl = '/user';

router.get(
    baseUrl,
    policy.isSignedIn,
    controller.detail
);

router.put(
    baseUrl,
    policy.isSignedIn,
    controller.update
);

module.exports = router;