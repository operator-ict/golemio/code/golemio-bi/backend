const packageJson = require('../../../package.json');

const status = (req, res, next) => {
    res.status(200).json({"app_name": packageJson.name, "status": "Up", "version": packageJson.version});
};

module.exports = {
    status
};
