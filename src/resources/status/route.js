const {Router} = require('express');
const controller = require('./controller');

const router = new Router();

const baseUrl = '/status';

router.get(
    baseUrl,
    controller.status
);

module.exports = router;
