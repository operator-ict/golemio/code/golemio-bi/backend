function toDto(metadataDao) {
    let result = {
        attachment:
            metadataDao.attachment && metadataDao.attachment.id
                ? {
                      id: metadataDao.attachment.id,
                      title: metadataDao.attachment.title,
                      name: metadataDao.attachment.name,
                      content_type: metadataDao.attachment.content_type,
                      size: metadataDao.attachment.size,
                      url: metadataDao.attachment.url,
                  }
                : undefined,
        tags: metadataDao.tags.map((tag) => {
            return {
                _id: tag.id,
                title: tag.title,
            };
        }),
        _id: metadataDao.id,
        route: metadataDao.route,
        client_route: metadataDao.client_route,
        type: metadataDao.type,
        component: metadataDao.component,
        title: metadataDao.title,
        description: metadataDao.description ?? "",
        data: metadataDao.data,
    };
    if (metadataDao.thumbnail_url) {
        result.thumbnail = {
            content_type: metadataDao.thumbnail_content_type,
            url: metadataDao.thumbnail_url,
        };
    }

    return result;
}

function toDao(metadataDto) {
    return {
        type: metadataDto.type,
        component: metadataDto.component,
        route: metadataDto.route,
        client_route: metadataDto.client_route,
        fa_icon: metadataDto.fa_icon, //zkontrolovalt k cemu slouzi
        title: metadataDto.title,
        description: metadataDto.description,
        data: metadataDto.data,
        metadataTags: metadataDto.tags.map((tag) => {
            return {
                tag_id: tag,
            };
        }),
    };
}

module.exports = { toDto, toDao };
