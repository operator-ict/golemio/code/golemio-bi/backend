const { Router } = require("express");
const { configLoader } = require("../accessrequest/configLoader");
const forms = require("./forms");
const controller = require("./controller");
const policy = require("../../modules/policy");
const APIError = require("../../modules/error");
const errorMessages = require("../../modules/error/messages");
const validation = require("../../modules/validation");
const checkErrors = require("../../modules/validation").checkErrors;
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage, limits: { fieldSize: configLoader.storage.fileMaxSize, fileSize: configLoader.storage.fileMaxSize } });

const router = new Router();
const baseUrl = "/metadata";

router.get(
    `/user/:userId${baseUrl}`,
    policy.isSignedIn,
    validation.checkId("userId"),
    (req, res, next) => {
        if (req.session.user.admin || req.session.user.id === parseInt(req.params.userId, 10)) {
            return next();
        }
        return next(new APIError(errorMessages.forbidden, 403));
    },
    forms.list,
    controller.list,
);

router.get(baseUrl, policy.isSignedIn, policy.isAdmin, forms.list, controller.adminList);

router.get(
    `${baseUrl}/:metadataId`,
    policy.isSignedIn,
    policy.isAdmin,
    forms.adminDetail,
    checkErrors,
    controller.adminDetail,
);

router.put(
    `${baseUrl}/:metadataId`,
    policy.isSignedIn,
    policy.isAdmin,
    forms.update,
    checkErrors,
    controller.update,
);

router.post(baseUrl, policy.isSignedIn, policy.isAdmin, forms.create, checkErrors, controller.create);

router.delete(
    `${baseUrl}/:metadataId`,
    policy.isSignedIn,
    policy.isAdmin,
    forms.delete,
    checkErrors,
    controller.delete,
);

router.put(
    `${baseUrl}/:metadataId/track`,
    policy.isSignedIn,
    forms.checkMetadataId,
    checkErrors,
    controller.trackViews,
);

router.put(
    `${baseUrl}/:metadataId/thumbnail`,
    policy.isSignedIn,
    policy.isAdmin,
    upload.single("file"),
    forms.checkMetadataId,
    checkErrors,
    controller.uploadThumbnail,
);

router.delete(
    `${baseUrl}/:metadataId/thumbnail`,
    policy.isSignedIn,
    policy.isAdmin,
    forms.checkMetadataId,
    checkErrors,
    controller.deleteThumbnail,
);

router.get(`${baseUrl}/:metadataId/thumbnail`, forms.checkMetadataId, checkErrors, controller.thumbnail);

router.get(
    `${baseUrl}/:metadataId/powerbi-embed-token`,
    policy.isSignedIn,
    forms.getPowerBIToken,
    checkErrors,
    controller.getPowerBIToken,
);

module.exports = router;
