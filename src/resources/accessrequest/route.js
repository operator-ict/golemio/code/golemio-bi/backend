const { Router } = require("express");
const router = new Router();
const { handleValidationError } = require("./forms");
const { sendFormMail } = require("../../modules/emailManager");
const getLogger = require("../../modules/logger").getLogger;

const baseUrl = "/accessrequest";

router.post(`${baseUrl}`, handleValidationError, (req, res, next) => {
    let htmlMsg = "<h1>Žádost o přístup - Golemio BI<h1>\n";
    htmlMsg += `<p>Uživatel webu Golemio BI (${req.body.name} ${req.body.surname}) poslal žádost o přístup:</p><br>`;
    htmlMsg += `<p>Uživatelské informace:</p>`;
    htmlMsg += `<p>Name: ${req.body.name}</p>`;
    htmlMsg += `<p>Surname: ${req.body.surname}</p>`;
    htmlMsg += `<p>E-mail: ${req.body.email}</p>`;
    htmlMsg += `<p>Organization: ${req.body.organization}</p>`;

    sendFormMail(htmlMsg)
        .then((result) => {
            getLogger("sendFormMail").info("The email was sent successfully");
            res.status(200).json({ isOk: true });
        })
        .catch((error) => {
            getLogger("sendFormMail").error("Server error, the email was not sent.", error);
            res.status(500).json({ isOk: false, errors: { server: `Error sending email.` } });
        });
});

module.exports = router;
