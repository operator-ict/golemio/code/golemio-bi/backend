const validator = require("validator");
const axios = require("axios");
const getLogger = require("../../modules/logger").getLogger;
const { configLoader } = require("./configLoader");

const handleValidationError = (req, res, next) => {
    const data = req.body;
    if (
        !data.hasOwnProperty("name") ||
        !data.hasOwnProperty("surname") ||
        !data.hasOwnProperty("organization") ||
        !data.hasOwnProperty("email")
    ) {
        res.status(400).json({
            isOk: false,
            errors: {
                server: "Bad Request.",
            },
        });
        return;
    }

    let errors = {};

    if (validator.isEmpty(data.name)) {
        errors.name = "Name is required";
    }
    if (validator.isEmpty(data.surname)) {
        errors.surname = "Surname is required";
    }
    if (validator.isEmpty(data.organization)) {
        errors.organization = "Organization is required";
    }
    if (validator.isEmpty(data.email)) {
        errors.email = "Email is required";
    }
    if (!validator.isEmail(data.email)) {
        errors.email = "Email is invalid";
    }

    if (Object.getOwnPropertyNames(errors).length) {
        getLogger("handleValidationError").error("Validation error.", errors);
        res.status(422).json({
            isOk: false,
            errors,
        });
    } else next();
};

module.exports = { handleValidationError };
