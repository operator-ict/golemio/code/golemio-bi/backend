const getLogger = require("../../modules/logger").getLogger;
const PostgresConnector = require("../../dataAccess/postgresConnector");
const storageService = require("../../dataAccess/storageService");
const path = require("path");

const update = async (req, res, next) => {
    const attachmentFile = req.file;
    const { attachmentTitle } = req.body;
    const { metadataId } = req.params;

    try {
        const newAttachmentData = {
            metadata_id: metadataId,
            title: attachmentTitle,
            name: attachmentFile.originalname,
            content_type: attachmentFile.mimetype,
            size: attachmentFile.size,
            url: `/metadata/${metadataId}/attachment`,
        };

        const suffix = path.extname(newAttachmentData.name);

        const t = await PostgresConnector.getConnection().transaction();
        let newStorageKey;
        try {
            const [attachment, created] = await PostgresConnector.getModel("attachments").findOrCreate({
                where: { metadata_id: metadataId },
                defaults: newAttachmentData,
                transaction: t,
            });

            newStorageKey = await storageService.uploadAttachment(attachmentFile.buffer, attachment.id, suffix);

            if (created) {
                attachment.s3_key = newStorageKey;
                await attachment.save({ transaction: t });
            } else {
                if (attachment.s3_key && attachment.s3_key !== newStorageKey) {
                    // in case of changed configuration new and old key may differ, so old attachment should be cleared. Otherwise upload overrides existing file.
                    await storageService.deleteObjectByKey(attachment.s3_key);
                }
                newAttachmentData.s3_key = newStorageKey;
                await PostgresConnector.getModel("attachments").update(newAttachmentData, {
                    where: { id: attachment.id },
                    transaction: t,
                });
            }
            await t.commit();
        } catch (err) {
            t.rollback();
            getLogger("AttachmentController").error("Updating attachment was not succefull.", err);
            if (newStorageKey) {
                await rollbackStorageUpload(newStorageKey);
            }

            return res.status(500).json({ isOk: false, errors: { server: `Error metadata updating.` } });
        }

        return res.json({ isOk: true });
    } catch (err) {
        return next(err);
    }
};

const deleteAttachment = async (req, res, next) => {
    const { metadataId } = req.params;
    try {
        const attachmentFromDb = await PostgresConnector.getModel("attachments").findOne({ where: { metadata_id: metadataId } });

        if (!attachmentFromDb && attachmentFromDb.s3_key != null)
            return res.status(404).json({ isOk: false, errors: { server: `File does not exist.` } });

        await storageService.deleteObjectByKey(attachmentFromDb.s3_key);
        await attachmentFromDb.destroy();

        return res.json({ isOk: true });
    } catch (err) {
        return next(err);
    }
};

const detail = async (req, res, next) => {
    const { metadataId } = req.params;
    try {
        const attachmentFromDb = await PostgresConnector.getModel("attachments").findOne({ where: { metadata_id: metadataId } });

        if (!attachmentFromDb && !attachmentFromDb.s3_key)
            return res.status(404).json({
                isOk: false,
                errors: { server: `File does not exist.` },
            });

        const fileData = await storageService.getObjectAsStream(attachmentFromDb.s3_key);
        res.setHeader("Content-Type", attachmentFromDb.content_type);
        res.set("Content-Disposition", 'attachment; filename="' + attachmentFromDb.name + '"');
        fileData.pipe(res);
    } catch (err) {
        return next(err);
    }
};

async function rollbackStorageUpload(key) {
    try {
        await storageService.deleteObjectByKey(key);
    } catch (err) {
        getLogger("attachment").error("Rollback after error was not successful", err);
    }
}

module.exports = {
    update,
    delete: deleteAttachment,
    detail,
};
