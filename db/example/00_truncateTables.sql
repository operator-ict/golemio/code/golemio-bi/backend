TRUNCATE
    "metadata-tag",
    "favourite-tiles",
    "user",
    "tag",
    "metadata";

ALTER SEQUENCE metadata_id_seq RESTART WITH 1;
ALTER SEQUENCE metadata_id_seq RESTART WITH 1;
ALTER SEQUENCE user_user_id_seq RESTART WITH 1;
