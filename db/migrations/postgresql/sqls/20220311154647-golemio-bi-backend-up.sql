-- public."user" definition

CREATE TABLE "user" (
    "id" uuid NOT NULL DEFAULT gen_random_uuid(),
	"user_id" integer NOT NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT user_pk PRIMARY KEY ("id"),
    CONSTRAINT user_un UNIQUE ("user_id")
);

-- public.tag definition

CREATE TABLE tag (
	"id" uuid NOT NULL DEFAULT gen_random_uuid(),
	"title" varchar NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT tag_pk PRIMARY KEY ("id")
);

-- public.metadata definition

CREATE TABLE metadata (
	"id" uuid NOT NULL DEFAULT gen_random_uuid(),
	"type" varchar NULL,
	"component" varchar NULL,
	"route" varchar NULL,
	"client_route" varchar NULL,
	"fa_icon" varchar NULL,
	"title" varchar NULL,
	"description" text NULL,
    "thumbnail_url" text NULL,
    "thumbnail_s3_key" text NULL,
    "thumbnail_content_type" text NULL,
	"data" json NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT metadata_pk PRIMARY KEY ("id"),
	CONSTRAINT metadata_un UNIQUE ("client_route")
);

-- public."metadata-tag" definition

CREATE TABLE "metadata-tag" (
	"metadata_id" uuid NOT NULL,
	"tag_id" uuid NOT NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT metadata_tag_fk FOREIGN KEY ("metadata_id") REFERENCES metadata("id") ON DELETE CASCADE,
	CONSTRAINT metadata_tag_fk_1 FOREIGN KEY ("tag_id") REFERENCES tag("id") ON DELETE CASCADE
);

-- public."favourite-tiles" definition

CREATE TABLE "favourite-tiles" (
	"user_id" uuid NOT NULL,
	"metadata_id" uuid NOT NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT favourite_tiles_fk FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE,
	CONSTRAINT favourite_tiles_fk_1 FOREIGN KEY ("metadata_id") REFERENCES metadata("id") ON DELETE CASCADE
);

-- public."attachments" definition

CREATE TABLE "attachments" (
	"id" uuid NOT NULL DEFAULT gen_random_uuid(),
	"metadata_id" uuid NOT NULL,
    "title" varchar NULL,
    "name" varchar NULL,
    "content_type" varchar NULL,
    "size" integer NULL,
    "url" varchar NOT NULL,
    "s3_key" varchar NULL,
	"updated_at" timestamptz NOT NULL DEFAULT now(),
	"created_at" timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT attachments_fk_1 FOREIGN KEY ("metadata_id") REFERENCES metadata("id") ON DELETE CASCADE
);
